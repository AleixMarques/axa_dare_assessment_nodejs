const express = require('express')
const api = express.Router()

const basicAuth = require('../middlewares/basicAuthParser')
const authorizer = require('../middlewares/authorizer')
const usersCtrl = require('../controllers/usersCtrl')


/* Get user by username. */
/**
* @route GET /users/name/{username}
* @group Users - Operations about users
* @param {string} username.path.required - Username of the user you want to retrieve info (e.g. Manning)
* @returns {object} 200 - The requested user information
* @returns {Error} 404 - User information not found
* @returns {Error} 503 - User service unavailable
* @returns {Error} 400 - Username not specified or invalid syntax
*/
api.get('/users/name/:username', basicAuth, authorizer.hasValidUserRole, usersCtrl)

/* Get user by user id. */
/**
* @route GET /users/id/{user-id}
* @group Users - Operations about users
* @param {string} user-id.path.required - User ID of the user you want to retrieve info (e.g. Manning)
* @returns {object} 200 - The requested user information
* @returns {Error} 404 - User information not found
* @returns {Error} 503 - User service unavailable
* @returns {Error} 400 - User ID not specified or invalid syntax
*/
api.get('/users/id/:uid', basicAuth, authorizer.hasValidUserRole, usersCtrl)


module.exports = api
