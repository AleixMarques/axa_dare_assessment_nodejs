const express = require('express')
const api = express.Router()

const basicAuth = require('../middlewares/basicAuthParser')
const authorizer = require('../middlewares/authorizer')
const policiesCtrl = require('../controllers/policiesCtrl')


/* Get policies by username. */
/**
* @route GET /policies
* @group Policies - Operations about policies
* @param {string} username.query.required - Username of the user you want to retrieve policies (e.g. Britney)
* @returns {object} 200 - The requested user policies
* @returns {Error} 404 - Policies information not found
* @returns {Error} 503 - Policy service unavailable
* @returns {Error} 400 - Username not specified or invalid syntax
*/
api.get('/policies', basicAuth, authorizer.isAdmin, policiesCtrl.getPoliciesByUsername)

/**
* @route GET /policies/{policy-number}/user
* @group Policies - Operations about policies
* @param {string} policy-number.path.required - Number of the policy you want to retrieve user info
* @returns {object} 200 - The user info linked to the requested policy number
* @returns {Error} 404 - Policy or linked user information not found
* @returns {Error} 503 - Policy or user service unavailable
* @returns {Error} 400 - Policy number not specified or invalid syntax
*/
api.get('/policies/:policyNumber/user', basicAuth, authorizer.isAdmin, policiesCtrl.getUserFromPolicyByNumber)


module.exports = api
