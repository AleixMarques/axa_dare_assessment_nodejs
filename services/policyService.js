const axios = require('axios')

const url = 'http://www.mocky.io/v2/580891a4100000e8242b75c5'

function getPolicies() {
  return axios.get(url)
}

function getPoliciesByClientId(clientId) {
  return new Promise((resolve, reject) => {
    getPolicies().then(res =>
      resolve(res.data.policies.filter(policy => policy.clientId === clientId))
    ).catch(() => reject('Unexpected error: Could not retrieve policies'))
  })
}

function getPolicyById(requestedPolicyNumber) {
  return new Promise((resolve, reject) => {
    getPolicies().then(res =>
      resolve(res.data.policies.filter(policy => policy.id === requestedPolicyNumber))
    ).catch(() => reject('Unexpected error: Could not retrieve policies'))
  })
}

module.exports = {
  getPoliciesByClientId,
  getPolicyById
}