const axios = require('axios')

const url = 'http://www.mocky.io/v2/5808862710000087232b75ac'

function getUsers() {
  return axios.get(url)
}

function getUsersByName(requestedUsername) {
  return new Promise((resolve, reject) => {
    getUsers().then(res =>
      resolve(res.data.clients.filter(client => client.name === requestedUsername))
    ).catch(() => reject('Unexpected error: Could not retrieve users'))
  })
}

function getUserById(requestedUserId) {
  return new Promise((resolve, reject) => {
    getUsers().then(res =>
      resolve(res.data.clients.filter(client => client.id === requestedUserId))
    ).catch(() => reject('Unexpected error: Could not retrieve users'))
  })
}

module.exports = {
  getUsersByName,
  getUserById
}