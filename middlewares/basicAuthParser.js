const userService = require('../services/userService')

function parseBasicAuth(req, res, next) {
  // Parse login and password from headers
  const b64auth = (req.headers.authorization || '').split(' ')[1] || ''
  
  // To avoid excluding password that contain ':'
  const strauth = Buffer.from(b64auth, 'base64').toString()
  const splitIndex = strauth.indexOf(':')
  const username = strauth.substring(0, splitIndex)
  const password = strauth.substring(splitIndex + 1)
  
  // We omit password checking, we only check for its existence
  if (username && password) {
    // Get all users and search for the user we need
    userService.getUsersByName(username).then(users => {
      // We consider that this would actually pass through some service that would 
      // retrieve the authenticated user info given an auth token, but we delegate 
      // this logic to this layer for simplicity.
      if (users.length) {
        req.user = users[0]
        next()
      } else {
        notAuthenticated(res)
      }
    }).catch(() => res.status(503).send('Service unavailable'))
  } else {
    notAuthenticated(res)
  }
}

function notAuthenticated(res) {
  res.set('WWW-Authenticate', 'Basic realm="401"')
  res.status(401).send('Authentication required.')
}


module.exports = parseBasicAuth