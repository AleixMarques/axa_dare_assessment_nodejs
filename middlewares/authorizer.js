function hasValidUserRole(req, res, next) {
  const user = req.user
  if (user.role === 'admin' || user.role === 'user') {
    next()
  } else {
    notAuthorized(res)
  }
}

function isAdmin(req, res, next) {
  if (req.user.role === 'admin') {
    next()
  } else {
    notAuthorized(res)
  }
}

function notAuthorized(res) {
  res.status(403).send('Not authorized to perform this request.')
}


module.exports = {
  hasValidUserRole,
  isAdmin
}