## AXA Dare Assessment NodeJS

### Usage

Once the app is running, navigate to the Welcome Page at `localhost:3000`. 

Use the **REST API documentation tool (Swagger UI)** to test the API manually and directly from the browser.

### Install

To install the package dependencies of the app, run the following on the project folder:

    npm run build

### Launch

To launch the app, run the following on the project folder:

    npm start

### Test

To run all the app tests, run the following on the project folder:

    npm test

#### Considerations

The decisions taken on some requirements of the assessment and other development aspects are explained through inline comments in the source code.

---

*Elliot Ribas Garcia - Sogeti Spain*
