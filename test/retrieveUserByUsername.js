const request = require('supertest')
const app = require('../app')

const password = 'asdasd'
const url = '/api/users/name/'

describe('Given an username retrieve the related user data', () => {
  it('user is not authenticated', (done) => {
    const noMatterUsername = 'test'
    request(app)
      .get(url + noMatterUsername)
      .expect(401, done)
  })
  
  it('user is authenticated but requested user does not exist', (done) => {
    const username = 'Barnett'
    const nonExistingUsername = 'UnknownUser'
    request(app)
      .get(url + nonExistingUsername)
      .auth(username, password)
      .expect(404, done)
  })
  
  it('user is authenticated but indicates an invalid username', (done) => {
    const username = 'Barnett'
    const invalidUsername = 'Test123'
    request(app)
      .get(url + invalidUsername)
      .auth(username, password)
      .expect(400, done)
  })
  
  it('user is authenticated as user and asks for itself', (done) => {
    const username = 'Barnett'
    request(app)
      .get(url + username)
      .auth(username, password)
      .expect(200, {
        id: 'a3b8d425-2b60-4ad7-becc-bedf2ef860bd',
        name: 'Barnett',
        email: 'barnettblankenship@quotezart.com',
        role: 'user'
      }, done)
  })
  
  it('user is authenticated as admin and asks for itself', (done) => {
    const username = 'Manning'
    request(app)
      .get(url + username)
      .auth(username, password)
      .expect(200, {
        id: 'e8fd159b-57c4-4d36-9bd7-a59ca13057bb',
        name: 'Manning',
        email: 'manningblankenship@quotezart.com',
        role: 'admin'
      }, done)
  })
  
  it('user is authenticated and asks for other user', (done) => {
    const username = 'Barnett'
    const requestedUser = 'Manning'
    request(app)
      .get(url + requestedUser)
      .auth(username, password)
      .expect(200, {
        id: 'e8fd159b-57c4-4d36-9bd7-a59ca13057bb',
        name: 'Manning',
        email: 'manningblankenship@quotezart.com',
        role: 'admin'
      }, done)
  })
})