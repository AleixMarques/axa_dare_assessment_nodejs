const request = require('supertest')
const app = require('../app')

const password = 'asdasd'
const url = '/api/users/id/'

describe('Given a user id retrieve the related user data', () => {
  it('user is not authenticated', (done) => {
    const noMatterUserId = 'test'
    request(app)
      .get(url + noMatterUserId)
      .expect(401, done)
  })
  
  it('user is authenticated but requested user does not exist', (done) => {
    const username = 'Barnett'
    const nonExistingUserId = '00000000-0000-0000-0000-000000000000'
    request(app)
      .get(url + nonExistingUserId)
      .auth(username, password)
      .expect(404, done)
  })
  
  it('user is authenticated but indicates an invalid user id', (done) => {
    const username = 'Barnett'
    const invalidUserId = '123456789'
    request(app)
      .get(url + invalidUserId)
      .auth(username, password)
      .expect(400, done)
  })
  
  it('user is authenticated as user and asks for itself', (done) => {
    const username = 'Barnett'
    const requestedUserId = 'a3b8d425-2b60-4ad7-becc-bedf2ef860bd'
    request(app)
      .get(url + requestedUserId)
      .auth(username, password)
      .expect(200, {
        id: 'a3b8d425-2b60-4ad7-becc-bedf2ef860bd',
        name: 'Barnett',
        email: 'barnettblankenship@quotezart.com',
        role: 'user'
      }, done)
  })
  
  it('user is authenticated as admin and asks for itself', (done) => {
    const username = 'Manning'
    const requestedUserId = 'e8fd159b-57c4-4d36-9bd7-a59ca13057bb'
    request(app)
      .get(url + requestedUserId)
      .auth(username, password)
      .expect(200, {
        id: 'e8fd159b-57c4-4d36-9bd7-a59ca13057bb',
        name: 'Manning',
        email: 'manningblankenship@quotezart.com',
        role: 'admin'
      }, done)
  })
  
  it('user is authenticated and asks for other user', (done) => {
    const username = 'Barnett'
    const requestedUserId = 'e8fd159b-57c4-4d36-9bd7-a59ca13057bb'
    request(app)
      .get(url + requestedUserId)
      .auth(username, password)
      .expect(200, {
        id: 'e8fd159b-57c4-4d36-9bd7-a59ca13057bb',
        name: 'Manning',
        email: 'manningblankenship@quotezart.com',
        role: 'admin'
      }, done)
  })
})