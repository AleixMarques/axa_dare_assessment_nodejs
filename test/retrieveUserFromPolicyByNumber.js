const request = require('supertest')
const app = require('../app')

const password = 'asdasd'
const url = policyNumber => `/api/policies/${policyNumber}/user`

describe('Given a policy number retrieve the linked user data', () => {
  it('user is not authenticated', (done) => {
    const noMatterPolicyNumber = 'test'
    request(app)
      .get(url(noMatterPolicyNumber))
      .expect(401, done)
  })

  it('user is authenticated as user and asks for the user linked to a policy number', (done) => {
    const username = 'Barnett'
    const requestedPolicyNumber = '7b624ed3-00d5-4c1b-9ab8-c265067ef58b'
    request(app)
      .get(url(requestedPolicyNumber))
      .auth(username, password)
      .expect(403, done)
  })

  it('user is authenticated as admin but indicates an invalid policy number', (done) => {
    const username = 'Manning'
    const invalidPolicyNumber = '123456789'
    request(app)
      .get(url(invalidPolicyNumber))
      .auth(username, password)
      .expect(400, done)
  })
  
  it('user is authenticated as admin but requested policy does not exist', (done) => {
    const username = 'Manning'
    const nonExistingPolicyNumber = '00000000-0000-0000-0000-000000000000'
    request(app)
      .get(url(nonExistingPolicyNumber))
      .auth(username, password)
      .expect(404, done)
  })
  
  it('user is authenticated as admin and asks for the user linked to a policy number', (done) => {
    const username = 'Manning'
    const requestedPolicyNumber = '7b624ed3-00d5-4c1b-9ab8-c265067ef58b'
    request(app)
      .get(url(requestedPolicyNumber))
      .auth(username, password)
      .expect(200, {  
        id: 'a0ece5db-cd14-4f21-812f-966633e7be86',
        name: 'Britney',
        email: 'britneyblankenship@quotezart.com',
        role: 'admin'
      }, done)
  })

})