const request = require('supertest')
const app = require('../app')

const password = 'asdasd'
const url = '/api/policies'

describe('Given an username retrieve the linked policies', () => {
  it('user is not authenticated', (done) => {
    const noMatterUsername = 'test'
    request(app)
      .get(url)
      .query({ username: noMatterUsername })
      .expect(401, done)
  })
  
  it('user is authenticated as user and asks for policies', (done) => {
    const username = 'Barnett'
    const requestedUsername = 'Britney'
    request(app)
      .get(url)
      .query({ username: requestedUsername })
      .auth(username, password)
      .expect(403, done)
  })

  it('user is authenticated as admin but indicates an invalid username', (done) => {
    const username = 'Manning'
    const invalidUsername = 'Test123'
    request(app)
      .get(url)
      .query({ username: invalidUsername })
      .auth(username, password)
      .expect(400, done)
  })

  it('user is authenticated as admin but requested user does not exist', (done) => {
    const username = 'Manning'
    const nonExistingUsername = 'UnknownUser'
    request(app)
      .get(url)
      .query({ username: nonExistingUsername })
      .auth(username, password)
      .expect(404, done)
  })
  
  it('user is authenticated as admin and asks for policies', (done) => {
    const username = 'Manning'
    const requestedUsername = 'Britney'
    const expectedFirstPolicy = {
      id: '7b624ed3-00d5-4c1b-9ab8-c265067ef58b',
      amountInsured: 399.89,
      email: 'inesblankenship@quotezart.com',
      inceptionDate: '2015-07-06T06:55:49Z',
      installmentPayment: true,
      clientId: 'a0ece5db-cd14-4f21-812f-966633e7be86'
    }
    request(app)
      .get(url)
      .query({ username: requestedUsername })
      .auth(username, password)
      .expect((res) => {
        if (JSON.stringify(expectedFirstPolicy) !== JSON.stringify(res.body[0])) {
          throw new Error('Incorrect policies were returned')
        }
      })
      .expect(200, done)
  })
})