const createError = require('http-errors')
const express = require('express')
const cookieParser = require('cookie-parser')
const logger = require('morgan')

const app = express()
const usersRouter = require('./routes/usersRoutes')
const policiesRouter = require('./routes/policiesRoutes')
const expressSwagger = require('express-swagger-generator')(app)

// Swagger UI setup
let options = {
  swaggerDefinition: {
    info: {
      description: 'This is a REST API documentation tool',
      title: 'Swagger',
      version: '2.0.0',
    },
    basePath: '/api',
    produces: [
      'application/json',
    ],
    schemes: ['http', 'https'],
    securityDefinitions: {
      basicAuth: {
        type: 'basic',
        in: 'header',
        name: 'Authorization',
        description: 'Basic Authentication via username:password',
        value: 'Basic '
      }
    },
    security: [
      {
        'basicAuth': []
      }
    ]
  },
  basedir: __dirname,
  files: ['./routes/*.js']
}
expressSwagger(options)

app.use(logger('dev'))
app.use(express.json())
app.use(express.urlencoded({ extended: false }))
app.use(express.static(__dirname + '/public'))
app.use(cookieParser())
app.use('/api', usersRouter, policiesRouter)

// catch 404 and forward to error handler
app.use((req, res, next) => next(createError(404)))

// error handler
app.use((err, req, res) => {
  // set locals, only providing error in development
  res.locals.message = err.message
  res.locals.error = req.app.get('env') === 'development' ? err : {}

  // render the error page
  res.status(err.status || 500)
  res.send()
})


module.exports = app
