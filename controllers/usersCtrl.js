const userService = require('../services/userService')

function getUser(req, res) {
  if (req.params.username && /^[a-z]([-']?[a-z]+)*$/i.test(req.params.username)) {
    getUserByName(req, res)
  } else if (req.params.uid && /^(?:[a-f0-9]{8})-(?:[a-f0-9]{4}-){3}(?:[a-f0-9]{12})$/.test(req.params.uid)) {
    getUserById(req, res)
  } else {
    res.status(400).send('Bad request: Search parameter not specified or invalid syntax.')
  }
}

function getUserByName(req, res) {
  // If user is the same as the authenticated user we do not need to call 
  // userService again since we already have the user info stored in the request
  const requestedUsername = req.params.username
  if (req.user.name === requestedUsername) {
    res.send(req.user)
  } else {
    userService.getUsersByName(requestedUsername).then(users => {
      if (users.length) {
        res.send(users[0])
      } else {
        res.status(404).send('User not found')
      }
    }).catch((err) => res.status(503).send(`Service unavailable. ${err}`))
  }
}

function getUserById(req, res) {
  // If user is the same as the authenticated user we do not need to call 
  // userService again since we already have the user info stored in the request
  const requestedUserId = req.params.uid
  if (req.user.id === requestedUserId) {
    res.send(req.user)
  } else {
    userService.getUserById(requestedUserId).then(users => {
      if (users.length) {
        res.send(users[0])
      } else {
        res.status(404).send('User not found')
      }
    }).catch((err) => res.status(503).send(`Service unavailable. ${err}`))
  }
}

module.exports = getUser