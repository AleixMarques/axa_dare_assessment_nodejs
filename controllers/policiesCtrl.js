const policyService = require('../services/policyService')
const userService = require('../services/userService')

function getPoliciesByUsername(req, res) {
  const clientName = req.query.username
  if (clientName && /^[a-z]([-']?[a-z]+)*$/i.test(clientName)) {
    userService.getUsersByName(clientName).then(users => {
      if (users.length) {
        policyService.getPoliciesByClientId(users[0].id).then(policies => {
          if (policies.length) {
            res.send(policies)
          } else {
            res.status(404).send('Policies not found')
          }
        }).catch((err) => res.status(503).send(`Service unavailable. ${err}`))
      } else {
        res.status(404).send('User not found')
      }
    }).catch((err) => res.status(503).send(`Service unavailable. ${err}`))
  } else {
    res.status(400).send('Bad request: Username not specified or invalid syntax.')
  }
}

function getUserFromPolicyByNumber(req, res) {
  const policyNumber = req.params.policyNumber
  if (policyNumber && /^(?:[a-f0-9]{8})-(?:[a-f0-9]{4}-){3}(?:[a-f0-9]{12})$/.test(policyNumber)) {
    policyService.getPolicyById(policyNumber).then(policies => {
      if (policies.length) {
        userService.getUserById(policies[0].clientId).then(users => {
          if (users.length) {
            res.send(users[0])
          } else {
            res.status(404).send('User not found')
          }
        }).catch((err) => res.status(503).send(`Service unavailable. ${err}`))
      } else {
        res.status(404).send('Policy not found')
      }
    }).catch((err) => res.status(503).send(`Service unavailable. ${err}`))
  } else {
    res.status(400).send('Bad request: Policy number not specified or invalid syntax.')
  }
}


module.exports = {
  getPoliciesByUsername,
  getUserFromPolicyByNumber
}